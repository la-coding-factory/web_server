# Web Server

## Comment bien démarrer

```
npm init -y
```

Installer les modules Express, Mongoose, Dotenv
```
npm i --save express mongoose dotenv
```

### Express

C'est un module permettant de créer un serveur web et gérer les différentes routes facilement

### Mongoose

C'est un "ODM" `Object Data Modeling` permettant de créer des `Schemas` et `Models` de donnée

### Dotenv

Une librairie permettant de créer des fichiers d'environnement facilement `.env`

## Architecture du projet

- /web
Ce dossier contiendra les fichiers relatifs au serveur web.

- /public
Ce dossier contiendra des fichiers servis statiquement par le serveur Web Express

- /modules

