import { config } from 'dotenv'
// config() va charger le fichier .env
config();

export default {
	HOST: process.env.HOST || '127.0.0.1',
	PORT: process.env.PORT || 10080,
	MONGO_URI: process.env.MONGO_URI || 'mongodb://localhost/db_node_paris_test',
  JWT_PRIVATE_KEY: process.env.JWT_PRIVATE_KEY || 'JWT Private Key',
}











/*
groupe1 - yaSMiFaHeHTDfy65TKA
groupe2

db.createUser(
  {
    user: "groupe4",
    pwd: passwordPrompt(),
    roles: [
      { role: "readWrite", db: "groupe4" }
    ]
  }
)
*/
