import mongoose from 'mongoose'

const schema = new mongoose.Schema({
	url: String,
	method: { type: String, default: 'GET', enum: ['GET', 'POST', 'DELETE'] },
	query: {
		type: mongoose.SchemaTypes.Mixed,  // pour Object
		default: {}
	}
}, {
	minimize: false,
	timestamps: true,
})

const Demo = mongoose.model('Demo', schema)

export default Demo

/*
const obj = {
	url: '',
	method: '',
	query: {},
}

*/
