import mongoose from 'mongoose'

const schema = new mongoose.Schema({
	email: String,
	password: String,
	level: { type: Number, default: 0 },

	confirmEmailToken: { type: String },

}, {
	minimize: false,
	timestamps: true,
})

const User = mongoose.model('User', schema)

export default User

/*
const obj = {
	url: '',
	method: '',
	query: {},
}

*/
