import mongoose from "mongoose";
import config from "../config.js";

export function mongoConnect() {
	// return promise
	return mongoose.connect(config.MONGO_URI)
}

export class Database {
	static async createUser(username, password, options) {
		const result = await mongoose.connection.db.addUser(username, password, options)
	}
}
