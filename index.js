import ExpressWebServer from './web/ExpressWebServer.js'
import IOServer  from './web/IOServer.js'
import config from './config.js'
import { mongoConnect } from './db/mongo.js'
import demoRouter from './modules/demo/demo.router.js'
import authRouter from './modules/auth/auth.router.js'

console.log('Connexion a mongo !');
// on se connecte mongo
mongoConnect()
	.then(() => {
		console.log('On start le serveur web')
		const eserver = new ExpressWebServer()
		const ioServer = new IOServer(eserver.server)
		eserver.listen(config.PORT, config.HOST)
		eserver.app.use(demoRouter)
		eserver.app.use(authRouter)
	})
	.catch(err => {
		console.error(err.message)
		process.exit(1)
	})

// import ExpressWebServer from "./web/ExpressWebServer.js";

// const server = new ExpressWebServer()
// server.init()
// server.listen(8080, '127.0.0.1')

// function logRequestMiddleware(req, res, next) {
// 	console.log(req.url, req.query)
// 	next();
// }

// server.app.use(logRequestMiddleware)



// server.app.get('/', (request, response) => {
// 	// request.method
// 	response.json({
// 		todo: {
// 			name: 'Hello ma todo',
// 			done: true
// 		}
// 	})
// })
