import { createJWT, verifyJwt } from './auth.services'

export async function login(req, res) {
	//
	const jwt = await createJWT('1', req.body.email, req.body.password)

	res.json({ jwt })

}

export async function verify(req, res) {
	const verified = await verifyJwt(req.body.jwt)

	res.json({ verified })
}

export async function signup(req, res) {
	//
	const list = await Demo.find({})

	res.json({
		list,
		demo: 'success'
	})
}
