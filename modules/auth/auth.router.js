import { Router } from "express";
import { login, verify } from "./auth.controller.js";

const router = Router()

router.post('/auth/verify', verify)
router.post('/auth/login', login)

export default router;
