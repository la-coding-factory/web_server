import User from '../../db/models/User'
import jwt from 'jsonwebtoken'
import config from '../../config.js'

const privateKey = config.JWT_PRIVATE_KEY;

const JwtSignOptions = {
	algorithm: 'HS256',
	expiresIn: '7d'
}

export async function createJWT(user_id, ...rest) {
	return new Promise((resolve, reject) => {
		jwt.sign({ user_id, ...rest }, privateKey, JwtSignOptions, (err, token) => {
			if (err) { return reject(err) }
			resolve(token)
		})
	})
}

export async function verifyJwt(token) {
	return new Promise((resolve, reject) => {
		jwt.verify(token, privateKey, (err, decoded) => {
			if (err) return reject(err);
			resolve(decoded)
		})
	})
}

export function getUserWithJWT(token) {
	return new Promise((resolve, reject) => {
		jwt.verify(token, privateKey, async (err, decoded) => {
			if (err) return reject(err);
			const user = await User.findOne({ _id: decoded.user_id })
			resolve(user)
		})
	})
}
