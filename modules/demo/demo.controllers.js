import Demo from '../../db/models/Demo.js'

export async function getDemo(req, res) {
	//
	const list = await Demo.find({})

	res.json({
		list,
		demo: 'success'
	})
}

export async function getOneDemo(req, res) {
	//
	const document = await Demo.findOne({ _id: req.params.id })

	res.json(document)
}

export async function postDemo(req, res) {

	await Demo.create({ url: '/demo', method: 'GET', query: req.query })

	console.log(req.body)
	// verif req.body
	// serviceDemo.add(req.body)

	res.json({
		demo: 'success'
	})
}
