import { Router } from "express";
import { getDemo, postDemo, getOneDemo } from "./demo.controllers.js";

const router = Router()

router.get('/demo', getDemo)
router.get('/demo/:id', getOneDemo)
router.post('/demo', postDemo)

export default router;
