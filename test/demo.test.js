import axios from 'axios'
import config from '../config.js'

axios.defaults.baseURL = `http://${config.HOST}:${config.PORT}`

async function getDemo() {
	const result = await axios.get('/demo')
	console.log(result.data)
}

async function getOneDemo() {
	const result = await axios.get('/demo/619cc978e9c33ff2a0f515ca')
	console.log(result.data)
}

async function postDemo() {
	const result = await axios.post('/demo')
	console.log(result.data)
}

getOneDemo()
