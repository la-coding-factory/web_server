import { createJWT, getUserWithJWT } from '../modules/auth/auth.services.js'

async function test() {
	const token = await createJWT('user_IIID')
	console.log(token)
	const decoded = await getUserWithJWT(token)
	console.log(decoded)
}

test()
