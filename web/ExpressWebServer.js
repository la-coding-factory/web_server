import http from "http"
import express from "express";
import cors from "cors";

class ExpressWebServer {

	constructor() {
		this.app = express()
		this.server = http.createServer(this.app)
	}

	init() {
		// ici on enregistre le middleware express.static pour servir le dossier "public" de manière statique
		this.app.use(express.static('../public'))
		// Ajouter des middlewares permettant de lire la donnée dans le body de la requête (POST)
		this.app.use(express.urlencoded({ extended: true })) // Gestion des formulaires classiques [POST]
		this.app.use(express.json()) // gestion du format JSON [POST]
		this.app.use(cors())
	}

	listen(port, host) {
		this.server.listen(port, host, () => {
			console.log(`Listening on http://${host}:${port}`)
		})
	}
}

export default ExpressWebServer
