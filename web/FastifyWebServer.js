import http from "http";
import Fastify from "fastify";

class FastifyWebServer {
	constructor() {
		this.app = Fastify({
			logger: true,
		})
	}

	listen(port, host) {
		this.app.listen(port, host, () => {
			console.log(`Listening on http://${host}:${port}`)
		})
	}

}

export default FastifyWebServer
