import { Server } from 'socket.io'

/**
 * Class qui crée un serveur socket.io
 * @property {io.Server} io
 * @class
 */
class IOServer {
	/** @type {IOServer} */
	static instance;

	io;

	/**
	 *
	 * @param {import('http').Server} httpServer
	 * @constructor
	 */
	constructor(httpServer) {
		if (IOServer.instance) {
			return IOServer.instance;
		}
		this.io = new Server(httpServer)
		this.io.on('connection', this.connHandler.bind(this))
		IOServer.instance = this;
	}

	/**
	 *
	 * @param {io.RemoteSocket} socketClient
	 */
	connHandler(socketClient) {
		console.log('client connected', socketClient.id)
	}


}

export default IOServer;
